%% 
%% Created in 2018 by Martin Slapak
%%
%% Based on file for NRP report LaTeX class by Vit Zyka (2008)
%%
%% Compilation:
%% >pdflatex report
%% >bibtex report
%% >pdflatex report
%% >pdflatex report

\documentclass[czech]{mvi-report}

\usepackage[utf8]{inputenc} 

\title{Detekce úniků pitné vody pomocí konvoluční neuronové sítě}

\author{Šimon Schierreich}
\affiliation{ČVUT - FIT}
\email{schiesim@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Úvod}
Světová zdravotnická organizace ve svém nedávno publikovaném reportu \cite{who_2019} ohledně přístupu k pitné vodě uvádí, že až $26\ \%$ lidské populace nemá přístup k upravené pitné vodě. Světového ekonomického fórum navíc problém nedostatku pitné vody označuje \cite{ganter_2015} za vůbec největší globální riziko do dalších desetiletí.

V dnešním výrazně technickém světě by neinformovaný pozorovatel mohl snadno dojít k názoru, že je, s přihlédnutím k výše uvedeným výstrahám světových kapacit, nedostatek pitné vody záležitost čistě regionální, která se západního světa příliš netýká. U nás má přeci každý k pitné vodě přístup a s vodou se jistě hospodaří ekonomicky.

První část tvrzení je sice pravdivá, druhá část již bohužel ne. Dle měření \cite{mrazek_2017} odborníků přijde v České republice každý rok jen z důvodu úniků z potrubí při cestě mezi vodárnou a kohoutkem v domácnostech vniveč  až $15\ \%$ vyprodukované pitné vody. Pro lepší představu se ve vodovodním potrubí každý den ztratí tolik vody, kolik by stačilo celé republice na pití po dobu devíti dní.

Jak je z uvedených dat patrné, pro vodárenské společnosti je poměrně problematické únikům pitné vody zabranit, a když už nastanou, tak je včas odhalit, a to hned z několika důvodů.

Jedním z důvodů je stáří sítě vodovodních potrubí. Ještě v roce 2016 se jen pod Prahou nacházelo okolo $100$ kilometrů potrubí vybudovaného v roce 1884 \cite{endrstova_havlicka_2016}. A podobné je to i v dalších městech. Výměna tohoto potrubí přitom není ve finančních možnostech měst.

S předchozím odstavcem souvisí i to, že i přes stále se snižující se cenu elektroniky je nasazení měřících zařízení do sítě potrubí poměrně drahé, proto jsou umístěna pouze na významných uzlech. Tím pádem je přesná lokalizace úniku značně problematická. 

Zcela určitě ne posledním, ale určitě významným, problémem je pak samotný algoritmus používaný k detekci úniku. Ten označí pravděpodobný únik a přibližnou lokalitu, nicméně únik potvrzuje, pokud voda nevyvěrá nad zem, až teréní pracovníci.

\section{Vstupní data}\label{ch:data}

Žádná reálná sada dat o únicích pitné vody z vodovodní sítě, bohužel, nebyla doposud žádnou společností veřejně publikována. Proto vznikl projekt LeakDB \cite{leakdb}, což je fyzikální simulace vodovodní sítě a úniků z ní. Její autoři se snažili o vytvoření standardní sady úloh, která by měla sloužit pro objektivní hodnocení vyvinutých algoritmů právě pro tento typ problémů.

Díky tomu, že data, nad kterými výkon algoritmu měříme, jsou uměle vytvořená, nebylo třeba je nijak výrazně čistit či jinak předzpracovávat.

Kompletní dataset je veřejně dostupný v repozitáři projektu LeakDB\footnote{https://github.com/KIOS-Research/LeakDB}.

\section{Současný algoritmus detekce úniků}

Jedním z nejpoužívanějších algoritmů na detekci úniků vody z potrubí je tzv. Minimum Night Flow (MNF) metoda \cite{w11010048}. Ta spočívá v měření nočního průtoku v potrubí a sledování, zda aktuální hodnota nepřekračuje nějakou předem danou maximální povolenou hodnotu.

Výsledky tohoto řešení nejsou příliš dobré. Jestliže se maximální povolená hodnota pohybuje blízko normálního průtoku, pak dokáže odhalit poměrně velké množství úniků, na druhou stranu o to častěji hlásí i falešné poplachy. Jestliže je tolerance velká, nebudou falešné poplachy tak časté, ale výrazně klesne počet odhalených úniků, zejména těch menších.

Zmíněná metoda má také problém s brzkým rozpoznáním úniku. To je způsobeno zejména tím, že analyzovány jsou pouze data z nočního provozu.

Kvůli zmíněným častým falešným poplachům se také v reálném provozu často čeká několik dní, než je únik potvrzen.

\section{Detekce úniků pomocí CNN}

\begin{figure*}[t!]
    \centering
 \includegraphics[width=0.8\textwidth]{img/arch.png}
 \caption{Architektura U-Net sítě pro detekci anomálií v časových řadách. Převzato z \cite{wen_keyes_2019}.}
 \label{fig:cnn_arch}
\end{figure*}

V rámci práce došlo k hledání úniků pitné vody z potrubí pomocí konvoluční neuronové sítě. Tato třída neuronových sítí je obvykle používána spíše k rozpoznávání obrázků, videa či muziky, nicméně Wen a Keyes představili v \cite{wen_keyes_2019} variantu této sítě, která najde využití pro detekci anomálií v časových řadách, což jsem se pokusil využít i pro řešení představeného problému.

\subsection{Architektura sítě}

Neuronová síť pro detekci anomálií v časových řadách navržená v \cite{wen_keyes_2019} se skládá z pěti kódovacích sekcí konvoluční vrstev. Každá sekce obsahuje dva bloky vrstev, přičemž každý z bloků obsahuje konvoluční vrstvu, normalizační vrstvu a ReLu vrstvu. Velikost kernelu je pro všechny konvoluční vrstvy rovna hodnotě $3$. Počet filtrů konvolučních vrstev se mezi sekcemi vždy zdvojnásobuje a první vrstva jich obsahuje právě $16$. Mezi kódovacími sekcemi jsou použity max pooling vrstvy s velikostí poolu rovnou $4$.

Poté následují čtyři dekódovací sekce, každá složená z jedné upsampling vrstvy a dvou bloků obsahujících konvoluční vrstvu, normalizační vrstvu a ReLu vrstvu. Upsampling poměr je znovu rovný $4$ a konvoluční vrstvy obsahují stejný počet filtrů, jako jejich kódovací protějšek, což činí celou síť symetrickou.

Důležitou vlastností U-Net sítí je vynechání kanálů mezi odpovídajícími kódovacími a dekódovacími sekcemi. Toho je dosaženo zřetězením výstupu každé max pooling vrstvy s výstupem z odpovídající upsampling vrstvy.

Jelikož se v našem případě jedná o binární klasifikaci, má výsledek hloubku $2$ a poslední sekce obsahuje konvoluční vrstvu s kernelem velikosti $1$ a softmax aktivační vrstvu. Jako ztrátová funkce je použita soft Dice loss a dále je používán optimalizátor Adam.

Kompletní schema navržené konvoluční sítě je uvedeno na obrázku \ref{fig:cnn_arch}.

\subsection{Implementace}

V předchozí kapitole představená síť byla, poměrně přímočaře, implementována pomocí frameworku Keras\footnote{\url{https://keras.io/}}. Jako backend pro Keras sloužila knihovna TensorFlow\footnote{\url{https://www.tensorflow.org/}}.

\section{Výkonnost}

Benchmark zmíněný v kapitole \ref{ch:data} obsahuje dvě simulace vodovodních sítí s úniky. Implementovaná neuronová síť byla naučena nad částí data-setu Hanoi, na zbytku pak došlo k testování. Výsledky jsou uvedeny v tabulce \ref{tab:results}.

\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|}
 \hline
 \textbf{Algoritmus} & $\mathbf{R_{TP}}$ & $\mathbf{R_{TN}}$ & $\mathbf{F_{tp,fp}}$ \\
 \hline
 Ideal & 100.00 & 100.00 & 100.00 \\
 \hline
  MNF 10\% & 68.98 & 66.96 & 50.01 \\
  \hline
  MNF 20\% & 39.90 & 99.58 & 56.49 \\
  \hline
  MNF 30\% & 32.87 & 99.91 & 49.36 \\
  \hline
  CNN & 49.94 & 2.81 & 5.24  \\
  \hline
 \end{tabular}

 \caption{Porovnání výkonu neuronové sítě a MNF metody nad data-setem \uv{Hanoi}.}
 \label{tab:results}

\end{table}

Jak je patrné, naučená neuronová síť dokázala relativně dobře rozpoznat výpadky. Výrazně špatně si ale vedla ve všech ostatních metrikách, zejména dělá velice často falešné poplachy.

\section{Závěr}

Představená neuronová síť se ukázala být, alespoň tak, jak byla implementována, o něco horší, než doposud studované metody.

Jelikož síť dává poměrně dobré výsledky v oblasti rozpoznávání úniků, stálo by za to pokusit se ji lépe natrénovat tak, aby se zlepšila i v ostatních parametrech.

Další práce v této oblasti by se také mohla zabývat například snahou odhalit kromě samotného úniku i jeho rozsah.

O potrubí také často známe některé detaily o jeho konstrukci, například průměry trubek. Mohlo by být zajímavé tyto informace do procesu zapojit.

S ohledem na praktický přínos podobných řešení by jistě stálo za zvážení i použití jiných typů neuronových sítí.

%\bibliographystyle{plain-cz-online}
\bibliography{reference}

\end{document}
